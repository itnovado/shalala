import { useShallow } from 'zustand/react/shallow'

import { MapStateToProps, MapActionsToProps, ShalalaBoundStore } from './types';

const useGlobalStore = <S extends object, T extends object = object>(
  useStore: ShalalaBoundStore<S>
) => <U extends object, V extends object>(
  mapState: MapStateToProps<S>,
  mapActions?: MapActionsToProps<T>
): Readonly<[U, V]> => {
  let storeActions = useStore.getActions();

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const value = useStore(useShallow(mapState)) as U;
  const actions = (!!mapActions
    ? mapActions(storeActions as any)
    : storeActions) as V;

  return [value, actions];
};

export default useGlobalStore;