import { StoreApi } from 'zustand';
import { UseBoundStore } from 'zustand';

export type Action<S, T> = (store: S, ...args: unknown[]) => unknown;

export type Actions = {
  [key: string]: object | (<S>(store: S, ...args: unknown[]) => unknown);
};

export type CustomSetState<S> = S | (() => S);

export type ShalalaInitializer<S> = {
  state: CustomSetState<S>;
  actions: Actions;
} & Partial<{
  [key: string]:
    | Partial<ShalalaInitializer<S>>
    | CustomSetState<S>
    | Actions
    | any;
}>;

export type MapStateToProps<
  S extends object = object,
  T extends object = object
> = (state: S) => T;

export type MapActionsToProps<
  T extends object = object,
  U extends object = object
> = (actions: T) => U;

export type ShalalaBoundStore<S extends unknown> = UseBoundStore<
  StoreApi<S>
> & {
  getActions: () => Actions;
  setState: <S>(state: S) => S | Partial<S> | void;
};

export type ShalalaStore<S extends object> = {
  setState: StoreApi<S>['setState'];
  getState: StoreApi<S>['getState'];
  set: StoreApi<S>['setState'];
  get: StoreApi<S>['getState'];
  api: StoreApi<S>;
};
