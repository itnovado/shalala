import { Actions } from './types';
import { ShalalaInitializer } from './types';

export function combineState<S>(store: ShalalaInitializer<S>) {
  let state = {} as any;

  if (typeof store !== 'object') {
    throw new Error('The store must be an object');
  }

  if (store.state) {
    return store.state instanceof Function
      ? (store.state as () => S)()
      : store.state;
  }

  for (const key of Object.keys(store)) {
    if (typeof store[key] === 'object') {
      if (store[key].state) {
        state[key] =
          store[key].state instanceof Function
            ? (store[key].state as () => S)()
            : store[key].state;
      } else {
        state[key] = combineState(store[key] as ShalalaInitializer<Partial<S>>);
      }
    }
  }

  return state as S;
}

export function combineActions<T>(store: ShalalaInitializer<T>) {
  let actions: any = {};

  if (typeof store !== 'object') {
    throw new Error('The store must be an object');
  }

  if (store.actions) {
    return store.actions;
  }

  for (const key of Object.keys(store)) {
    if (typeof store[key] === 'object') {
      actions[key] = !!store[key].actions
        ? store[key].actions
        : combineActions(store[key] as any);
    } else {
      console.warn(`Invalid type definition for action ${key}`);
    }
  }

  return actions as Actions;
}

export default function combineStore<S>(store: ShalalaInitializer<S>) {
  return {
    state: combineState(store),
    actions: combineActions(store),
  };
}
