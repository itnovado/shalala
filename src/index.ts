import { create, StateCreator } from 'zustand';

import bind from './bind';
import immer from './plugins/immer';
import { compose } from './compose';
import actionable from './plugins/actionable';
import useGlobalStore from './useGlobalStore';
import { connect, customConnect } from './connect';
import { Actions, ShalalaBoundStore, ShalalaInitializer } from './types';

type Plugins<S extends object> = StateCreator<S>[];
type Options<S extends object> = {
  plugins?: Plugins<S> | any;
};

const createStore = <S extends object, T extends object = object>(
  initialState: ShalalaInitializer<S>,
  initialActions?: Actions,
  options: Options<S> = {}
) => {
  const { state, actions } = bind(initialState, initialActions);

  const plugins = options.plugins ?? [];

  const globalStore = compose(...plugins)(
    actionable(state, actions)
  ) as StateCreator<S>;

  const useStore = create<S>()(globalStore);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const useGlobal = useGlobalStore<S, T>(useStore as ShalalaBoundStore<S>);

  const resetStore = () => useStore.setState(state);

  return Object.assign(useGlobal, {
    useStore: useStore as ShalalaBoundStore<S>,
    useGlobal,
    resetStore,
    connect: customConnect(useStore as ShalalaBoundStore<S>),
  });
};

export { bind, immer, connect, actionable, createStore, ShalalaBoundStore };

export default createStore;
