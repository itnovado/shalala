import React, { ComponentType } from 'react';
import { useShallow } from 'zustand/react/shallow';

import { MapActionsToProps, MapStateToProps, ShalalaBoundStore } from './types';

type GetProps<C> = C extends ComponentType<infer P> ? P : never;

type Omit<T, K> = Pick<T, Exclude<keyof T, K>>

const connect = <
  MappedState extends object,
  State extends object,
  MappedActions extends object,
  Actions extends object,
  OwnProps extends object
>(
  useStore: ShalalaBoundStore<State>,
  mapState: MapStateToProps<State, MappedState>,
  mapActions: MapActionsToProps<Actions, MappedActions>
) => <C extends ComponentType<GetProps<C>>>(
  WrappedComponent: C
): ComponentType<
  JSX.LibraryManagedAttributes<
    C,
    Omit<GetProps<C>, keyof MappedState | keyof MappedActions> & OwnProps
  >
> => {
  const ConnectedComponent = (props: any) => {
    const state = useStore(useShallow(mapState)) as MappedState;
    const storeActions = !!useStore.getActions ? useStore.getActions() : {};
    const actions = (!!mapActions
      ? mapActions(storeActions as any)
      : storeActions) as MappedActions;

    const wrapperName = 'connectedWithGlobalHoc';

    WrappedComponent.displayName = `${wrapperName}(${getDisplayName(
      WrappedComponent
    )})`;

    return <WrappedComponent {...props} {...state} {...actions} />;
  };

  return ConnectedComponent;
};

const customConnect = <State extends object, Actions extends object = object>(
  useStore: ShalalaBoundStore<State>
) => <MappedState extends object, MappedActions extends object, OwnProps extends object = object>(
  mapState: MapStateToProps<State, MappedState>,
  mapActions: MapActionsToProps<Actions, MappedActions>
) =>
  connect<MappedState, State, MappedActions, Actions, OwnProps>(
    useStore,
    mapState,
    mapActions
  );

function getDisplayName(WrappedComponent: ComponentType<any>) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export { connect, customConnect };
