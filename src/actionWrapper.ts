import { ShalalaStore } from './types';

const actionWrapper = <S extends object>(
  store: ShalalaStore<S>,
  originalFunction: Function
) => (...args: unknown[]) => {
  const result = originalFunction(...args);

  if (result instanceof Function) {
    store.setState(result);
  }

  return result;
};

export default actionWrapper;
