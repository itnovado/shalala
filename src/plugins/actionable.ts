import { StateCreator } from 'zustand';
import { Actions, ShalalaStore } from '../types';

import associateActions from '../associateActions';

type ActionableMiddelware = <S extends object>(
  state: S,
  actions?: Actions
) => StateCreator<S>;

const actionable: ActionableMiddelware = (state, actions) => (
  set,
  get,
  api
) => {
  let _store: ShalalaStore<typeof state> = {
    setState: set,
    getState: get,
    set,
    get,
    api,
  };

  const handler = {
    get(target: typeof _store, property: string) {
      if (property === 'state') {
        return get();
      }

      return (target as any)[property];
    },
  };

  let store = new Proxy(_store, handler);

  if (typeof actions === 'object') {
    (api as any).getActions = () => associateActions(store, actions);
  }

  return state;
};

export default actionable;
