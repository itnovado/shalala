import { produce } from 'immer';
import { StateCreator, StoreApi } from 'zustand';

type ImmerMiddelware = <T extends object>(
  config: StateCreator<T>
) => StateCreator<T>;

const immer: ImmerMiddelware = config => (set, get, api) => {
  type T = ReturnType<typeof config>;

  const setState: StoreApi<T>['setState'] = (partial, replace) => {
    const nextState =
      typeof partial === 'function'
        ? produce(partial as (state: T) => T)
        : partial;

    return set(nextState as T, replace);
  };

  return config(setState, get, api);
};

export default immer;
