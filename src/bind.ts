import { Actions } from './types';
import { ShalalaInitializer } from './types';

import combineStore from './combineStore';

const bind = <S>(
  initialState: S | ShalalaInitializer<S>,
  actions?: Actions
) => {
  let state: S;

  if (typeof initialState === 'object' && !actions) {
    const combined = combineStore<S>(initialState as ShalalaInitializer<S>);

    state = combined.state as S;
    actions = combined.actions;
  } else {
    state = initialState as S;
  }

  return {
    state,
    actions,
  };
};

export default bind;
