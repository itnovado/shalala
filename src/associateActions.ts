import actionWrapper from './actionWrapper';
import { Actions, ShalalaStore } from './types';

const associateActions = <S extends object>(
  store: ShalalaStore<S>,
  actions: Actions
) => {
  let associatedActions: any = {};

  for (const key of Object.keys(actions)) {
    if (actions[key] instanceof Function) {
      const originalFunction = (actions[key] as Function).bind(null, store);

      associatedActions[key] = actionWrapper(store, originalFunction);
    } else if (actions[key] instanceof Object) {
      associatedActions[key] = associateActions(store, actions[key] as Actions);
    }
  }

  return associatedActions;
};

export default associateActions;
